require "ruby-fifo"
require "notify-send-wrapper"


module DwmRubyBarCtl
  DEBUG=false
  fifofile = "/tmp/dwmrubybar"
  @@writer = Fifo.new(fifofile, :w, :nowait)
  
  def self.set m,v=nil
    NotifySend.go "DwmRubyBarCtl: #{m}" if DEBUG
    if v
      @@writer.puts "#{m}:#{v}"
    else
      @@writer.puts m
    end
  end
end

