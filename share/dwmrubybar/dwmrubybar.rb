require "ruby-fifo"
require "notify-send-wrapper"

require "/usr/local/share/dwmde/dwmde-code.rb"
require "/usr/local/share/dwmde/dwmde-config.rb"

module DwmRubyBar
  DEVEL=Dir.pwd.include? "Devel"
  DEBUG_TIME=false
  DEBUG_JOURNAL=false
  DEBUG_FIFO=false
  DEBUG_MODULE=true
  DEBUG_GENERAL=true
  NO_DEBUG_CLOCK=true
 
  MPATH = "#{__dir__}/modules/*"
  # puts "MPATH = #{MPATH}" if DEBUG_GENERAL

  @@separator="    "
  @@h = {}
  @@arr = []          # Per tenere i campi in ordine: contiene solo nomi dei elementi
  @@cycles = {}       # Per tenere traccia i campi che devono ciclicamente ricaricarsi
  @@journalexp = {}
  @@modules_mtime = {}
  @@pids = []
  @@hidden = true     # Come default iniziamo con moduli nascosti
  @@force_show = false


  fifofile = "/tmp/dwmrubybar"

  @@reader = Fifo.new(fifofile, :r, :nowait)
  @@writer = Fifo.new(fifofile, :w, :nowait)

  class BarModule
    def initialize opz={}
      @cache = secure_check
      @enabled = true
      @name = self.class.to_s.split("::")[1]
      DwmRubyBar.add self
      DwmRubyBar.set_cycle @name, opz[:cycle] if opz[:cycle] && opz[:cycle] > 0
      DwmRubyBar.set_regexp @name, opz[:regexp] if opz[:regexp]
    end
    attr_accessor :cache
    attr_reader :enabled
    def secure_check v=nil
      if v == "Disable"
        disable
        return nil
      elsif v == "Enable"
        enable
        return nil
      end
      begin
        check(v)
      rescue Exception => e
        exit if e.to_s == "SIGHUP"
        e.to_s
      end
    end
    def hidden?
      false
    end
    def disable
      @enabled = false
    end
    def enable
      @enabled = true
      DwmRubyBar.force_show
    end
  end # BarModule

  def self.add c
    c_name = c.class.to_s.split("::")[1]
    @@arr << c_name unless @@h.keys && @@h.keys.include?(c_name)
    @@h[c_name] = c
  end

  def self.set c_name,v=:check
    return nil unless @@h[c_name]
    if v == :check
      newcache = @@h[c_name].secure_check
    else
      newcache = @@h[c_name].secure_check v
    end
    return false if @@h[c_name].cache == newcache
    @@h[c_name].cache = newcache
    true
  end

  def self.set_cycle c_name, num
    return nil if num == 0
    @@cycles[num] ||= []
    @@cycles[num] << c_name  unless @@cycles[num].include? c_name
  end

  def self.set_regexp c_name, re
    return nil unless re.class == Regexp
    @@journalexp[re] ||= []
    @@journalexp[re] << c_name unless @@journalexp[re].include? c_name
  end
  def self.force_show
    @@force_show = true
  end

  ## Accessors
  def self.arr
    @@arr
  end
  def self.elements
    @@h
  end
  def self.writer
    @@writer
  end
  def self.modules_mtime
    @@modules_mtime
  end
  def self.pids
    @@pids
  end
  def self.hidden
    @@hidden
  end
  def self.hidden= v
    @@hidden = v
  end
  # Visualization
  def self.to_s
    s = ""
    @@arr.each{|e|
      next unless @@h[e]
      next if @@hidden && @@h[e].hidden?
      next unless @@h[e].enabled
      if @@hidden && @@h[e].cache && @@h[e].methods.include?(:short)
        if ![nil,""].include?(@@h[e].short)
          s += @@separator unless s == ""
          s += @@h[e].short
        end
      elsif @@h[e] && @@h[e].cache
        s += @@separator unless s == ""
        s += @@h[e].cache
      end
    }
    s
  end

  def self.put_on_screen
    puts "#{to_s}" if DEBUG_GENERAL
    system "xsetroot -name \"#{to_s}\""
    @@force_show = false
  end

  # Initialization
  def self.load_modules
    Dir[MPATH].sort.each{|n|
      @@modules_mtime[n] = File.stat(n).mtime
      puts "#{n}: Start loading..."
      t = Time.now
      load n
      puts "1st load: #{n} after #{Time.now-t}" if DEBUG_MODULE
    }
    puts "Modules loaded" if DEBUG_GENERAL
  end

  def self.run
    # Time updates
    pids << fork do
      i = 1
      begin
        puts "Start time updates" if DEBUG_GENERAL
        while true
          @@cycles.keys.each {|c|
            if i%c==0
              @@cycles[c].each{|m|
                puts "Cycle #{i}:#{m}" if DEBUG_TIME && (NO_DEBUG_CLOCK ? m != "Clock" : true)
                @@writer.puts m
              }
            end
          }
          i += 1
          i = 0 if i == 86400 # 24h
          sleep 1
          print "T" if DEBUG_TIME
        end
      rescue Interrupt,SignalException
        exit
      end
    end

    # Journal updates
    pids << fork do
      j = IO.popen("journalctl -f -n 0")
      begin
        puts "Start journal updates" if DEBUG_GENERAL
        while true
          l = j.gets.chomp
          print "J" if DEBUG_JOURNAL
          @@journalexp.keys.each {|exp|
            if l =~ exp
              @@journalexp[exp].each {|cl|
                @@writer.puts "#{cl}:#{l}"
                puts "By Journal: #{l}" if DEBUG_JOURNAL
              }
            end
          }
        end
      rescue Interrupt,SignalException
        exit
      end
    end

    # Fifo updates
    begin
      puts "Start FIFO updates" if DEBUG_GENERAL
      put_on_screen
      while true
        v = @@reader.gets.chomp.force_encoding('UTF-8')
        print "F" if DEBUG_FIFO
        r = if v.include? ":"
          m,*v = v.split ":"
          puts "By Fifo:#{m} <- #{v.join(":")}" if DEBUG_FIFO
          self.set m,v.join(":")
        else
          puts "By Fifo:#{v} <- check" if DEBUG_FIFO && (NO_DEBUG_CLOCK ? v != "Clock" : true)
          self.set v, :check
        end
        put_on_screen if r || @@force_show
      end
    rescue Interrupt, SignalException
      Process.wait
      `xsetroot -name "DwmRubyBar interotto "`
    end
  end
end

