module DwmRubyBar
  class WakeUpMonitor < BarModule
    def check v=nil
      return nil if v == nil
      DwmRubyBar.arr.each {|cl|
        next if cl == "WakeUpMonitor"
        DwmRubyBar.writer.puts "#{cl}"
      }
      nil
    end
  end
  WakeUpMonitor.new({regexp:/ACPI: Waking up from system sleep state/})
end

