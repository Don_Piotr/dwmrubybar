module DwmRubyBar
  class Updates < BarModule
    def diffkernel newer,older
      n = newer.split(".")
      o = older.split(".")
      r = []
      0.upto(2){|i|
        r[i] = n[i].to_i - o[i].to_i
        r[i] = 0 if r[i] < 0
      }
      return "K" if r == [0,0,0]
      "K(#{r.join(".")})"
    end
    def check arg=nil
      return nil if system "pgrep yay -a"
      if arg==nil
        @num = ""
        @k = ""
        p = fork do
          begin
            num = 0
            yay = `yes n | yay -Su 2>&1 `
            num = $2.to_i if yay =~ /(Pacchetti|Packages) \((\d+)\) /
            k = false
            yay = `yes n | yay -Ss linux | grep -E "/linux.*\\(Installed:" | grep -v -E "(docs|headers|firmware)"`.chomp
            unless yay == ""
              uname = `uname -r`.chomp
              type = nil
              if uname =~ /(-[A-Za-z]+)$/
                # testato con lts e zen
                type = $1
              elsif uname =~ /-arch\d+-\d+/
                type = ""
              else
                type = false
              end
              unless type == false
                yay =~ /(^|\n)\w+\/linux#{type} ([\w\.-]+) \(.*\) \(Installed:? ?(.*)\)($|\n)/
                unless $3 == ""
                  k = diffkernel($2,$3)
                end
              end
            end       
            DwmRubyBar.writer.puts "Updates:#{$$}:#{num}:#{k}"
            exit
          rescue Interrupt,SignalException
            exit
          end
        end # fork
        return nil
      else
        p, @num, @k = arg.split(":")
        Process.waitpid(p.to_i,0)
        if @num.to_i > 0 || @k != "false"
          s = " "
          s += @num unless @num == "0"
          s += @k unless @k == "false"
          s
        else
          nil
        end
      end
    end
    def short
      return nil if @num=="" && @k==""
      " " + (@num!="0" ? @num : "") + (@k!="false" ? "K" : "")
    end
  end
  Updates.new({cycle: 3600})
end

