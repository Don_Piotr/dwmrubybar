module DwmRubyBar
  class NewKernel < BarModule
    def check arg=nil
      current = `uname -r`.chomp
      branch = current.split("-").last
      branch = "" unless ["lts","zen"].include? branch
      f = Dir["/boot/vmlinuz*#{branch}"]
      return "Errore module newkernel" if f == []
      installed = `file #{f.last}`.split[8]
      return " Nuovo kernel" if current != installed
      nil
    end
  end
  NewKernel.new({cycle: 3600})
end

