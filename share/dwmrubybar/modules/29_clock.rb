module DwmRubyBar
  class Clock < BarModule
    def check arg=nil
      t = Time.now
      s = ["dom ","lun ","mar ","mer ","gio ","ven ","sab "][t.wday]
      s += t.day.to_s
      s += [nil," gen "," feb "," mar "," apr "," mag "," giu "," lug "," ago "," set "," ott "," nov "," dic "][t.mon]
      if t.hour < 10
        s += "0" + t.hour.to_s
      else
        s += t.hour.to_s
      end
      if t.min < 10
        s += ":0" + t.min.to_s
      else
        s += ":" + t.min.to_s
      end
      s
    end
  end
  Clock.new({cycle: 15})
end

