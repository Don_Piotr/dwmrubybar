module DwmRubyBar
  class ModuleReloader < BarModule
    def check v=nil
      Dir[MPATH].sort[1..-1].each{|m|
        if DwmRubyBar.modules_mtime[m] && DwmRubyBar.modules_mtime[m] < File.stat(m).mtime
          DwmRubyBar.modules_mtime[m] = File.stat(m).mtime
          load m
          NotifySend.go "Modulo #{m} ricaricato", {time: 5000, urgency: :low}
          puts "Reloader: #{m}" if DwmRubyBar::DEBUG_MODULE
        end
      }
      nil
    end
  end
  ModuleReloader.new({cycle:30})
end

