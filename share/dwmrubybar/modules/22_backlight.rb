module DwmRubyBar
  class Backlight < BarModule
    def check arg=nil
      f = `light -G -r`
      return nil if f.length > 6
      "  "+f.chomp
    end
    def hidden?
      DwmRubyBar.hidden
    end
  end
  Backlight.new()
end

