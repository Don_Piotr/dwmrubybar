module DwmRubyBar
  class Screensaver < BarModule
    def check arg=nil
      s = DwmDE::Dpms.check_screensaver
      d = DwmDE::Dpms.check_dpms
      if s && d
        @hideable = true
        return "  " 
      end
      @hideable = false
      return "  " if s || d
      return "  "
    end
    def hidden?
      @hideable
    end
  end
  Screensaver.new({cycle:3600})
end

