module DwmRubyBar
  class Mouse < BarModule
    def check arg=nil
      return nil unless system "which synclient 2>/dev/null 1>&2"
      stato = `synclient | grep TouchpadOff`
      return "ﳶ  " unless stato =~ /0/
      return "ﳶ  "
    end
    def hidden?
      DwmRubyBar.hidden
    end
  end
  Mouse.new()
end

