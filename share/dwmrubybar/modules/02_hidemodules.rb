module DwmRubyBar
  class HideModules < BarModule
    def check v=nil
      if v == "hide"
        DwmRubyBar.hidden = true
      elsif v == "show"
        DwmRubyBar.hidden = false
      elsif v == "switch"
        DwmRubyBar.hidden = !DwmRubyBar.hidden
      end
      DwmRubyBar.force_show
      nil
    end
  end
  HideModules.new
end

