require "psych"

module DwmRubyBar
  class ProcessMonitor < BarModule
    PROGRAMS = {"syncthing" => "S",
                "obs" => "O",
                "yay" => " ",
                "mbsync" => " ",
    }
    @@prefix = ""
    def check arg=nil
      s = ""
      PROGRAMS.each_key {|k|
        s += PROGRAMS[k] unless `pgrep #{k} -x` == ""
      }
      if s == ""
        s = nil
      end
      s
    end
    def hidden?
      DwmRubyBar.hidden
    end
  end
  ProcessMonitor.new({cycle: 30})
end

