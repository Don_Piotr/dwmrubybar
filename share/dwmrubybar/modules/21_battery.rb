module DwmRubyBar
  class Battery < BarModule
    IconCritical = " ﮙ "
    IconDischarging = " "
    IconCharging = "  "
    IconFull = ""

    def initialize opz={}
      @bat_path = Dir["/sys/class/power_supply/BAT*"]
      if @bat_path == []
        @bat_path = false
      else
        @bat_path = @bat_path.first
      end
      super opz
    end
    def check arg=nil
      return nil unless @bat_path
 
      @status = IO.readlines(@bat_path+"/status")[0].chomp
      @level = IO.readlines(@bat_path+"/capacity")[0].chomp

      case @status
      when "Discharging"
        if @level.to_i < 10
          NotifySend.go "Attacca la corrente", {summary:"Battery Widget", time: 5000, urgency: :critical, icon: :battery}
          `paplay /usr/share/sounds/freedesktop/stereo/bell.oga`
          return DwmRubyBar::Battery::IconCritical+"#{@level}%"
        else
          return DwmRubyBar::Battery::IconDischarging+"#{@level}%"
        end
      when "Charging"
        return DwmRubyBar::Battery::IconCharging+"#{@level}%"
      when "Full"
        return DwmRubyBar::Battery::IconFull
      else
        return "#{@status} #{@level}%"
      end
    end
    def hidden?
      if @status == "Discharging" and @level.to_i <= 50
        false
      else
        DwmRubyBar.hidden
      end
    end
  end
  Battery.new({cycle: 30})
end

