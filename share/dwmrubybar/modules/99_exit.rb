module DwmRubyBar
  class Exit < BarModule
    def check v=nil
      if v=="yes"
        DwmRubyBar.pids.each {|p|
          Process.kill("SIGHUP", p)
        }
        Process.wait
        `xsetroot -name "È stato mandato Exit:yes al DwmRubyBar "`
        Process.kill("SIGHUP", 0)
      end
      nil
    end
  end
  Exit.new
end

