module DwmRubyBar
  class Volume < BarModule
    # Per sapere i nomi delle tue schede audio eseguisci: pactl list cards
    SOUND_CARDS = {"pci-0000_00_01.1" => "HDMI",
                   "pci-0000_00_14.2" => " ",
                   "usb-Burr-Brown_from_TI_USB_Audio_CODEC-00" => "Mixer ",
                   "usb-C-Media_Electronics_Inc._USB_Audio_Device-00" => "UGreen ",
                   "bluez_sink.E8_07_BF_06_0C_29.a2dp_sink" => "Aukey ",
                   "default" => "al."}
    SOUND_ON   = "  "
    SOUND_MUTE = " ﱝ "

    def mute?
      return true if `pamixer --get-mute`.chomp == "true"
      false
    end
    def get_volume
      `pamixer --get-volume`.chomp
    end
    def check arg=nil
      s = " "
      DwmDE::Audio.get_sinks
      name = DwmDE::Audio.get_default_sink.name
      newname = nil
      SOUND_CARDS.each_key {|k|
        if name.scan(k) != []
          newname = k
          break
        end
      }
      newname = SOUND_CARDS["default"] unless newname
      s += SOUND_CARDS[newname]
      @mute = mute?
      @vol = get_volume
      return s + SOUND_MUTE if @mute
      s + SOUND_ON + @vol
    end
    def short
      return " " + SOUND_MUTE if @mute
      " " + SOUND_ON + @vol
    end
  end
  Volume.new
end

