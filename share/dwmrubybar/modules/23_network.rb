module DwmRubyBar
  class Network < BarModule
    IFCONNECTED = [/enp\ds\d/,/eno\d/,/wlo\d/,/wlp\ds\d/]
    IFVPN = [/tun\d/]
    @connected = false
    def check arg=nil
      @vpn = false
      ips = `ip a | grep "scope global"`
      @connected = ifs_search IFCONNECTED, ips
      @vpn = ifs_search IFVPN, ips
      s = "  "
      s += @connected? " " : " "
      s += "(+vpn)" if @connected && @vpn
    end
    def ifs_search ifs,ips
      r = false
      ifs.each {|ifregex|
        if ips =~ ifregex
          r = true
          break
        end
      }
      return r
    end
    def hidden?
      @connected
    end
  end
  Network.new({cycle:60,regexp:/wpa_supplicant(.*)CTRL-EVENT-(DIS)?CONNECTED/})
end

