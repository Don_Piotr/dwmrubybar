module DwmRubyBar
  class GetInfo < BarModule
    def putinfo s
      if STDOUT.tty?
        puts "--- Get Info module ---"
        puts s
        puts "-----------------------"
      else
        NotifySend.go s, {summary: "Get Info module"}
      end
    end
    def check v=nil
      return nil if v == nil
      s = "Check #{v}:\n"
      case v
      when "Modules"
         s += DwmRubyBar.arr.join("\n")
      when "Enabled"
        DwmRubyBar.arr.each{|m|
          s += m + "\n" if DwmRubyBar.elements[m].enabled
        }
      when "Disabled"
        DwmRubyBar.arr.each{|m|
          s += m + "\n" unless DwmRubyBar.elements[m].enabled
        }
      end
      putinfo s
      nil
    end
  end
  GetInfo.new()
end

