module DwmRubyBar
  class NewMails < BarModule
    # This work with mbsync

    MAILBOXES = { N: "#{ENV["HOME"]}/Mails/netc",
                  Y: "#{ENV["HOME"]}/Mails/yahoo",
                  G: "#{ENV["HOME"]}/Mails/gmail",
                  S: "#{ENV["HOME"]}/Mails/segretario",
                  C: "#{ENV["HOME"]}/Mails/curia"}


    def numxmb_to_s
      str = ""
      @num_x_mb.each{|k,v|
        str += k.to_s+v.to_s+" " if v > 0
      }
      str
    end
    def total_num
      total = 0
      @num_x_mb.values.each{|v|
        total += v.to_i
      }
      total
    end
    def check arg=nil
      @num_x_mb = {}
      MAILBOXES.each{|k,dir|
        next unless File.exist?(dir)
        mails = `find "#{dir}" -type f`.split("\n")
        mails.map! {|m|
          unless m =~ /([Tt]rash|[Bb]ulk|[Ss]pam)/
            if m =~ /\/([\w\s]+\/[\w\s]+)\/new\//
              $1
            else
              # Se non è nuova non ci interesa
              nil
            end
          else
            # Se è Spam o Cestino non ci interesa
            nil
          end
        }
        @num_x_mb[k] = mails.compact.length
      }
      return " 0" if total_num == 0
      " #{numxmb_to_s}"
    end
    def short
      t = total_num
      return nil if t == 0
      " #{t}"
    end
  end
  NewMails.new({cycle:1800})
end

