module DwmRubyBar
  class NewMailsMailnag < BarModule
    def check arg=nil
      @lastarg = arg
      return nil if arg == nil
      " #{arg}"
    end
    def short
      return nil if @lastarg == "0"
      t = @lastarg.split(" ")
      t.map!{|a|
        a.split(":")[1].to_i
      }

      " #{t.inject(0, :+)}"
    end
  end
  NewMailsMailnag.new
end

