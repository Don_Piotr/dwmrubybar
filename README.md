# DwmRubyBar - My statusbar for DWM writed in Ruby

My DwmRubyBar has grown enough to be separate repository.

## Dependences:
...

# Dwmrubybar

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'dwmrubybar'
```

And then execute:

    $ bundle install

Or install it yourself as:

    $ gem install dwmrubybar

## Usage

Make sure that rubygems' executables are in your $PATH. Then run statusbar by:

    $ dwmrubybar

You can add more modules in `lib/modules` directory.
You can change ProcessMonitor's symbols in `lib/conf/processmonitor.yml` file.

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
