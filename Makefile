# dwmrubybar
# See LICENSE file for copyright and license details.

VERSION = 1.2

# paths
PREFIX = /usr/local

install:
	chmod 755 ./bin/*
	mkdir -p ${DESTDIR}${PREFIX}/bin
	cp -f ./bin/* ${DESTDIR}${PREFIX}/bin/
	mkdir -p ${DESTDIR}${PREFIX}/share
	cp -rf ./share/dwmrubybar ${DESTDIR}${PREFIX}/share

uninstall:
	rm -f ${DESTDIR}${PREFIX}/bin/dwmrubybar
	rm -rf ${DESTDIR}${PREFIX}/share/dwmrubybar

